var express = require('express');
var bodyParser = require('body-parser');
var app = express()
app.use(bodyParser.json())

var requestJson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancomapr/collections"
var apiKey = "apiKey=4s3VhLTX39KjrLINjZJanMD5aSctw0YX"
var clienteMlab = null;

app.get('/apitechu/v5/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body)
      }
  })
})

app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  var filtro = "&f={'first_name':1, 'last_name':1, '_id':0}"
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + filtro + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length > 0)
           res.send(body[0])
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
  })
})

app.post('/apitechu/v5/login', function(req, res) {
  var nuevo = {"email":req.headers.email,
               "password":req.headers.password}

  console.log(nuevo.email)
  console.log(nuevo.password)

  var query = 'q={"email":"' + nuevo.email + '","password":"' + nuevo.password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) {
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
            var cambio = '{"$set":{"logged":true}}'
            clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"login":"ok", "id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
            })
        }
        else {
          res.status(404).send('No existe usuario')
        }
      }
  })
})

app.post('/apitechu/v5/logout', function(req, res) {
  var id = req.headers.id

  console.log(id)

  var query = 'q={"id":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1)
        {
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
            var cambio = '{"$set":{"logged":false}}'
            clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].id})
            })
        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
      }
  })
})

app.get('/apitechu/v5/cuentas/id', function(req, res) {
  var idcliente = req.headers.idcliente

  var query = 'q={"idcliente":' + idcliente + '}'
  var filtro = "&f={'iban':1, '_id':0}"
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + filtro + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  })
})

app.get('/apitechu/v5/cuentas/movimientos', function(req, res) {
  var iban = req.headers.iban
  var encontrado = false

  var query = 'q={"iban":' + iban + '}'
  var filtro = "&f={'movimientos':1, '_id':0}"
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + filtro + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      console.log(body)
      res.send(body)
    }
  })
//  for (var i = 0; i < cuentas.length; i++) {

//    if (ibanheader == cuentas[i].iban) {
//      encontrado = true
//       res.send(cuentas[i].movimientos)
//     }
//  }
//  if (encontrado == false) {
//    res.send("inexistente")
//  }
})



var port = process.env.PORT || 3000
var fs = require('fs')

app.listen(port)

console.log("API escuchando en el puerto" + port)

var usuarios = require('./usuarios.json');

app.get('/apitechu/v1', function(req, res) {
//  console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function(req, res) {
  res.send(usuarios)
})

app.post('/apitechu/v1/usuarios', function(req, res) {
  var nuevo = {"first_name":req.headers.first_name,
               "country":req.headers.country}
  usuarios.push(nuevo)
  console.log(req.headers)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
       console.log(err)}
    else {
      console.log("Fichero guardado")
    }
  })
  res.send("Alta OK")
})

app.delete('/apitechu/v1/usuarios/:id', function(req, res) {
  usuarios.splice(req.params.id-1, 1)
  res.send("Usuario Borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res) {
  console.log("Parámetros")
  console.log(req.params)
  console.log("QueryStrings")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)
  res.send("OK")
})

app.post('/apitechu/v2/usuarios', function(req, res) {
//  var nuevo = {"first_name":req.headers.first_name,
//               "country":req.headers.country}
  var nuevo = req.body
  usuarios.push(nuevo)
  console.log(req.body)
  const datos = JSON.stringify(usuarios)
  fs.writeFile("./usuarios.json", datos, "utf8", function(err) {
    if (err) {
       console.log(err)}
    else {
      console.log("Fichero guardado")
    }
  })
  res.send("Alta OK")
})

app.post('/apitechu/v3/usuarios/login', function(req, res) {
  var nuevo = {"email":req.headers.email,
               "password":req.headers.password}
  var idusuario = 0;
  console.log(nuevo.email)
  console.log(nuevo.password)

  for (var i = 0; i < usuarios.length; i++) {
    if (nuevo.email == usuarios[i].email && nuevo.password == usuarios[i].password)
       {
        idusuario = usuarios[i].id
        console.log("Encontrado")
        usuarios[i].logged = true
        break;
       }
  }

  if (idusuario !=0)
    res.send({"id":idusuario})
  else {
    res.send({"encontrado":"no"})
  }

})

app.post('/apitechu/v3/usuarios/logout', function(req, res) {
  var idusuario = req.headers.id
  var usuario_logado = false

//  usuarios[idusuario-1].logged = false

  for (var i = 0; i < usuarios.length; i++) {
    if (usuarios[i].id == idusuario && usuarios[i].logged == true)
       {
        usuario_logado = true
        usuarios[i].logged = false
        break;
       }
  }

  if (usuario_logado == true)
      res.send({"logout":"si","id":idusuario})
  else {
      res.send({"logout":"no", "msj":"el usuario logado no habia iniciado sesion"})
  }

})

var cuentas = require('./cuentas.json');
var arrayiban = []

app.get('/apitechu/v1/cuentas', function(req, res) {
  for (var i = 0; i < cuentas.length; i++) {
    arrayiban.push(cuentas[i].iban)
  }
res.send(arrayiban)
})


app.get('/apitechu/v1/cuentas/movimientos', function(req, res) {
  var ibanheader = req.headers.iban
  var encontrado = false

  for (var i = 0; i < cuentas.length; i++) {

    if (ibanheader == cuentas[i].iban) {
      encontrado = true
       res.send(cuentas[i].movimientos)
     }
  }
  if (encontrado == false) {
    res.send("inexistente")
  }
})

var arrayibancliente = []

app.get('/apitechu/v1/cuentas/id', function(req, res) {
  var idclienteheader = req.headers.idcliente
  for (var i = 0; i < cuentas.length; i++) {
    if (idclienteheader == cuentas[i].idcliente) {
       arrayibancliente.push(cuentas[i].iban)
    }
  }
res.send(arrayibancliente)
})
