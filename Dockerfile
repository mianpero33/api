FROM node
WORKDIR /apitechu
ADD . /apitechu
RUN npm install
VOLUME ["/logs"]
EXPOSE 3000
CMD ["npm", "start"]
